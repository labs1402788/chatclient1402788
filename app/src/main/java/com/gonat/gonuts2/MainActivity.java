package com.gonat.gonuts2;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private Handler uiHandler = new Handler() {
        public void handleMessage(Message msg) {
        if (msg.what == 0){
            TextView result = (TextView)findViewById(R.id.chatView);
            result.append((String)msg.obj + "\n"); }
    } };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Socket sokka = new Socket();
        String rndstr;

        MessageReceiver mr = new MessageReceiver(sokka, uiHandler);

        Thread t1 =new Thread(mr);
        t1.start();
        setContentView(R.layout.activity_main);
        final TextView chatti = (TextView) findViewById(R.id.chatView);
        final EditText edit = (EditText)findViewById(R.id.edit_message);
        final Button b1 = (Button) findViewById(R.id.sendMessage);

        b1.setOnClickListener(new View.OnClickListener() {

       public void onClick(View v) {
           String teksti;
           teksti = edit.getText().toString();
           MessageSender ms = new MessageSender(sokka, teksti);
           Thread t = new Thread(ms);
           t.start();
           edit.setText("");
       }
       });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
