package com.gonat.gonuts2;


import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

public class MessageReceiver implements Runnable {
InetSocketAddress ipa;
    Socket rs;
    BufferedReader in;
    String v1 ="";
    Handler handler;

    public MessageReceiver(Socket b, Handler h){
        this.rs = b;
        this.ipa = new InetSocketAddress("192.168.1.84", 100);
        handler = h;
    }

    public void run(){
        try {
            this.rs.connect(ipa);
            Log.e("try", "try");
            in =new BufferedReader(new InputStreamReader(rs.getInputStream()));

            while(true){
               v1 = in.readLine();

                while(v1 != null){
                    Message msg = handler.obtainMessage();
                    msg.obj = v1;
                    msg.what = 0;

                    if(!v1.equals("")){
                        handler.sendMessage(msg);
                    }
                    v1 = in.readLine();

                }
                in.close();
            }


        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Catch", "Catch");
        }
    }
}
